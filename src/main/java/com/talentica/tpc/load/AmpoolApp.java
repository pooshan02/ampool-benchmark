package com.talentica.tpc.load;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

import com.talentica.tpc.table.CustomerTable;
import com.talentica.tpc.table.LineItemTable;
import com.talentica.tpc.table.NationTable;
import com.talentica.tpc.table.OrdersTable;
import com.talentica.tpc.table.PartSuppTable;
import com.talentica.tpc.table.PartTable;
import com.talentica.tpc.table.RegionTable;
import com.talentica.tpc.table.SupplierTable;
import com.talentica.tpc.table.Table;
import com.talentica.utility.TablePathUtil;

public class AmpoolApp {

	static JavaSparkContext jsc;

	static Table[] tables = { new CustomerTable(), new LineItemTable(), new NationTable(), new OrdersTable(),
			new PartSuppTable(), new PartTable(), new RegionTable(), new SupplierTable() };
	public static void main(String[] args) {

		String master = null;
		String appName = null;
		String locatorHost = null;
		String locatorPort = null;
		String batchSize = "1000";

		String tpchTablePath = null;

		if (args.length == 6) {
			master = args[0];
			appName = args[1];
			locatorHost = args[2];
			locatorPort = args[3];
			batchSize = args[4]; // default 1000
			tpchTablePath = args[5];

		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<locatorHost> , <locatorPort>,<batchSize>,<tpch-parent-path>");
			System.exit(-1);
		}
		
		String[] paths = { TablePathUtil.CUSTOMER.getAbsolutePath(tpchTablePath),
				TablePathUtil.LINEITEM.getAbsolutePath(tpchTablePath),
				TablePathUtil.NATION.getAbsolutePath(tpchTablePath),
				TablePathUtil.ORDERS.getAbsolutePath(tpchTablePath),
				TablePathUtil.PARTSUPP.getAbsolutePath(tpchTablePath),
				TablePathUtil.PART.getAbsolutePath(tpchTablePath), TablePathUtil.REGION.getAbsolutePath(tpchTablePath),
				TablePathUtil.SUPPLIER.getAbsolutePath(tpchTablePath)};

		SQLContext sqlContext = SparkSession.builder().appName(appName).master(master).getOrCreate().sqlContext();

		Map<String, String> options = new HashMap<>();
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));
		options.put("ampool.batch.size", batchSize);

		for (int i = 0; i < tables.length; i++) {
			DataLoader.createTable(sqlContext, paths[i], tables[i], options);
		}

	}

}
