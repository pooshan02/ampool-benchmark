/**
 * 
 */
package com.talentica.tpc.load;

import org.apache.kudu.spark.kudu.KuduContext;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import com.talentica.tpc.table.CustomerTable;
import com.talentica.tpc.table.LineItemTable;
import com.talentica.tpc.table.NationTable;
import com.talentica.tpc.table.OrdersTable;
import com.talentica.tpc.table.PartSuppTable;
import com.talentica.tpc.table.PartTable;
import com.talentica.tpc.table.RegionTable;
import com.talentica.tpc.table.SupplierTable;
import com.talentica.tpc.table.Table;
import com.talentica.utility.TablePathUtil;

/**
 * @author pooshans
 *
 */
public class KuduApp {

	static Table[] tables = { new CustomerTable(), new LineItemTable(), new NationTable(), new OrdersTable(),
			new PartSuppTable(), new PartTable(), new RegionTable(), new SupplierTable() };

	public static void main(String[] args) {

		String master = null;
		String appName = null;
		String kuduMaster = null;
		String kuduPort = null;
		int numberOfBucket = 16;
		String tableName = "ALL"; // To save all the tables
		String tpchTablePath = null;

		if (args.length >= 5) {
			master = args[0];
			appName = args[1];
			kuduMaster = args[2];
			kuduPort = args[3];
			tpchTablePath = args[4];
			if (args.length > 5) {
				numberOfBucket = Integer.valueOf(args[5]);
			}
			System.out.println("Number of partitioned for Kudu :: " + numberOfBucket);
			if (args.length > 6) {
				tableName = args[6];
			}
		} else {
			System.err.printf("Please provide spark <master-URL> , <app-name> ,<kuduMaster> , <kuduPort>,<tpchTablePath>"
					+ "<optional-numberOfBucket-default-16> ,<table-name %n default : %n\tALL %n else %n"
					+ "\tCUSTOMER %n\tLINEITEM %n\tNATION %n\tORDERS %n\tPARTSUPP %n\tPART %n\tREGION %n\tSUPPLIER >");
			System.exit(-1);
		}
		
		String[] paths = { TablePathUtil.CUSTOMER.getAbsolutePath(tpchTablePath),
				TablePathUtil.LINEITEM.getAbsolutePath(tpchTablePath),
				TablePathUtil.NATION.getAbsolutePath(tpchTablePath),
				TablePathUtil.ORDERS.getAbsolutePath(tpchTablePath),
				TablePathUtil.PARTSUPP.getAbsolutePath(tpchTablePath),
				TablePathUtil.PART.getAbsolutePath(tpchTablePath), TablePathUtil.REGION.getAbsolutePath(tpchTablePath),
				TablePathUtil.SUPPLIER.getAbsolutePath(tpchTablePath)};

		SparkConf sparkConf = new SparkConf().setAppName(appName).setMaster(master);
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
		KuduContext context = new KuduContext(kuduMaster + ":" + kuduPort, sc.sc());

		switch (tableName) {
		case "CUSTOMER":
			DataLoader.createTable(sparkSession.sqlContext(), paths[0], tables[0], context, numberOfBucket);
			break;
		case "LINEITEM":
			DataLoader.createTable(sparkSession.sqlContext(), paths[1], tables[1], context, numberOfBucket);
			break;
		case "NATION":
			DataLoader.createTable(sparkSession.sqlContext(), paths[2], tables[2], context, numberOfBucket);
			break;
		case "ORDERS":
			DataLoader.createTable(sparkSession.sqlContext(), paths[3], tables[3], context, numberOfBucket);
			break;
		case "PARTSUPP":
			DataLoader.createTable(sparkSession.sqlContext(), paths[4], tables[4], context, numberOfBucket);
			break;
		case "PART":
			DataLoader.createTable(sparkSession.sqlContext(), paths[5], tables[5], context, numberOfBucket);
			break;
		case "REGION":
			DataLoader.createTable(sparkSession.sqlContext(), paths[6], tables[6], context, numberOfBucket);
			break;
		case "SUPPLIER":
			DataLoader.createTable(sparkSession.sqlContext(), paths[7], tables[7], context, numberOfBucket);
			break;
		case "ALL":
		default:
			for (int i = 0; i < tables.length; i++) {
				DataLoader.createTable(sparkSession.sqlContext(), paths[i], tables[i], context, numberOfBucket);
			}
			break;
		}

		sc.close();
	}

}
