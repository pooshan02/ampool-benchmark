package com.talentica.tpc.load;

import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

import com.talentica.tpc.table.CustomerTable;
import com.talentica.tpc.table.LineItemTable;
import com.talentica.tpc.table.NationTable;
import com.talentica.tpc.table.OrdersTable;
import com.talentica.tpc.table.PartSuppTable;
import com.talentica.tpc.table.PartTable;
import com.talentica.tpc.table.RegionTable;
import com.talentica.tpc.table.SupplierTable;
import com.talentica.tpc.table.Table;
import com.talentica.utility.TablePathUtil;

public class HdfsApp {

	static Table[] tables = { new CustomerTable(), new LineItemTable(), new NationTable(), new OrdersTable(),
			new PartSuppTable(), new PartTable(), new RegionTable(), new SupplierTable() };

	public static void main(String[] args) {

		String master = null;
		String appName = null;
		String hdfsPath = null; // hdfsPath = "hdfs://<host>:<port>/
		String isORCFormat = "N";
		String tpchTablePath = null;

		if (args.length == 5) {
			master = args[0];
			appName = args[1];
			hdfsPath = args[2];
			isORCFormat = args[3];
			tpchTablePath = args[4];
		} else {
			System.err.println("Please provide spark <master-URL> , <app-name> ,<hdfsPath> , <isORCFormat>,<tpch-parent-path>");
			System.exit(-1);
		}
		String[] paths = { TablePathUtil.CUSTOMER.getAbsolutePath(tpchTablePath),
				TablePathUtil.LINEITEM.getAbsolutePath(tpchTablePath),
				TablePathUtil.NATION.getAbsolutePath(tpchTablePath),
				TablePathUtil.ORDERS.getAbsolutePath(tpchTablePath),
				TablePathUtil.PARTSUPP.getAbsolutePath(tpchTablePath),
				TablePathUtil.PART.getAbsolutePath(tpchTablePath), TablePathUtil.REGION.getAbsolutePath(tpchTablePath),
				TablePathUtil.SUPPLIER.getAbsolutePath(tpchTablePath)};
		SparkSession session;
		if (isORCFormat.equalsIgnoreCase("Y")) {
			session = SparkSession.builder().enableHiveSupport().appName(appName).master(master).getOrCreate();
		} else {
			session = SparkSession.builder().appName(appName).master(master).getOrCreate();
		}
		SQLContext sqlContext = session.sqlContext();
		for (int i = 0; i < tables.length; i++) {
			DataLoader.createTable(sqlContext, paths[i], tables[i], hdfsPath, isORCFormat);
		}

	}

}
