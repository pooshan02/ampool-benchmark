package com.talentica.tpc.load;

import java.util.Map;

import org.apache.kudu.client.CreateTableOptions;
import org.apache.kudu.spark.kudu.KuduContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.Table;

import scala.collection.JavaConversions;

public class DataLoader {

	/**
	 * To save data in csv format in Ampool
	 * 
	 * @param sqlContext
	 * @param filePath
	 * @param table
	 * @param ampoolOptions
	 */
	public static void createTable(SQLContext sqlContext, String filePath, Table table,
			Map<String, String> ampoolOptions) {
		ampoolOptions.put("ampool.partitioning.column", table.partitionColumn());
		Dataset<Row> dataset = sqlContext.read().format("com.databricks.spark.csv").schema(table.getSchema())
				.option("header", "false").option("delimiter", "|").load(filePath);
		dataset.write().format("io.ampool").options(ampoolOptions).save(table.getTableName());
	}

	/**
	 * To save data in ORC format in HDFS.
	 * 
	 * @param sqlContext
	 * @param filePath
	 * @param table
	 * @param hdfsPath
	 */
	public static void createTable(SQLContext sqlContext, String filePath, Table table, String hdfsPath,
			String isORCFormat) {
		Dataset<Row> dataset = sqlContext.read().format("com.databricks.spark.csv").schema(table.getSchema())
				.option("header", "false").option("delimiter", "|").load(filePath);
		if (isORCFormat.equalsIgnoreCase("Y")) {
			dataset.write().orc(hdfsPath + table.getTableName());
		} else {
			dataset.write().save(hdfsPath + table.getTableName());
		}
	}

	public static void createTable(SQLContext sqlContext, String filePath, Table table, KuduContext context,
			int buckets) {
		System.out.println("Kudu table :: " + table.getTableName());
		try {
			Dataset<Row> dataset = sqlContext.read().format("com.databricks.spark.csv").schema(table.getSchema())
					.option("header", "false").option("delimiter", "|").load(filePath);
			CreateTableOptions kuduTableOption = new CreateTableOptions();
			kuduTableOption.addHashPartitions(table.getPrimaryKeys(), buckets);
			if (context.tableExists(table.getTableName())) {
				context.deleteTable(table.getTableName());
			}
			context.createTable(table.getTableName(), table.getSchema(),
					JavaConversions.asScalaBuffer(table.getPrimaryKeys()), kuduTableOption);
			context.insertRows(dataset, table.getTableName());
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Cound not create table in Kudu:: " + table.getTableName());
			throw ex;
		}
	}
}
