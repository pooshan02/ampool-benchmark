/**
 * 
 */
package com.talentica.tpc.table;

/**
 * @author pooshans
 *
 */
public enum TableEnum {

	/** The TPC PART Table name **/
	PART("PART"),

	/** The TPC SUPPLIER Table name **/
	SUPPLIER("SUPPLIER"),

	/** The TPC PARTSUPP Table name **/
	PARTSUPP("PARTSUPP"),

	/** The TPC CUSTOMER Table name **/
	CUSTOMER("CUSTOMER"),

	/** The TPC ORDERS Table name **/
	ORDERS("ORDERS"),

	/** The TPC LINEITEM Table name **/
	LINEITEM("LINEITEM"),

	/** The TPC NATION Table name **/
	NATION("NATION"),

	/** The TPC REGION Table name **/
	REGION("REGION"),
	
	
	
	/** The BigDataBench ORDER_ITEM Table name **/
	OS_ORDER_ITEM("OS_ORDER_ITEM"),
	
	/** The BigDataBench ORDER Table name **/
	OS_ORDER("OS_ORDER");

	private String name;

	private TableEnum(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
