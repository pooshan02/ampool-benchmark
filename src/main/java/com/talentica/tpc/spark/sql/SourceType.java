/**
 * 
 */
package com.talentica.tpc.spark.sql;

/**
 * @author pooshans
 *
 */
public enum SourceType {
	Ampool("ampool"), HdfsOrc("hdfsorc"),Alluxio("alluxio"),Kudu("kudu");

	private String name;

	private SourceType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
