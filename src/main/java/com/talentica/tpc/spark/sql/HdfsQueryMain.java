package com.talentica.tpc.spark.sql;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import com.talentica.utility.BenchmarkMode;

public class HdfsQueryMain {

	public static void main(String[] args) {

		String master = null;
		String appName = null;
		String hdfsPath = null;
		int numberOfIterations = 0;
		int sqlId = -1;
		// int tpcSqlId = -1;
		int partitions = 0; // Default 0 will let the spark recognize the number
							// of partition itself otherwise it enforce the
							// number of partition be done by spark.
		int totalTpcSqlId = 22;
		BenchmarkMode benchmarkMode = BenchmarkMode.NON_CACHE;
		if (args.length >= 4) {
			master = args[0];
			appName = args[1];
			hdfsPath = args[2];
			numberOfIterations = Integer.parseInt(args[3]);
			if (args.length >= 5) {
				sqlId = Integer.parseInt(args[4]);
			}
			if (args.length >= 6 && args[5].equalsIgnoreCase("y")) {
				benchmarkMode = BenchmarkMode.CACHE;
			}
			if (args.length >= 7) {
				partitions = Integer.parseInt(args[6]);
			}

		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<hdfsPath>,<numberOfIterations>,<optional-sqlId>,<optional-cache-(y/n)-default-n>,<optional-partitions-default-0>");
			System.exit(-1);
		}

		System.out.println("Benchmark Mode :: " + benchmarkMode.mode());

		switch (benchmarkMode) {
		case NON_CACHE:
			if (sqlId != -1) {
				SparkConf sparkConf = new SparkConf().setAppName(appName + "-sqlId-" + sqlId).setMaster(master);
				JavaSparkContext sc = new JavaSparkContext(sparkConf);
				SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
				QueryExecutor executor = new QueryExecutor(sparkSession, hdfsPath, SourceType.HdfsOrc);
				executor.prepare(TPCHSqlId.getById(sqlId), partitions);
				executor.execute(numberOfIterations);
				sc.close();
				break;
			}
			for (int itr = 0; itr < numberOfIterations; itr++) {
				for (TPCHSqlId tpcSqlId : TPCHSqlId.values()) {
					SparkConf sparkConf = new SparkConf().setAppName(appName + "-sqlId-" + tpcSqlId).setMaster(master);
					JavaSparkContext sc = new JavaSparkContext(sparkConf);
					SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
					QueryExecutor executor = new QueryExecutor(sparkSession, hdfsPath, SourceType.HdfsOrc);
					executor.prepare(tpcSqlId, partitions);
					executor.execute(numberOfIterations);
					sc.close();
				}
			}
			break;
		case CACHE:
			SparkConf sparkConf = new SparkConf().setAppName(appName).setMaster(master);
			JavaSparkContext sc = new JavaSparkContext(sparkConf);
			SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
			for (TPCHSqlId tpcSqlId : TPCHSqlId.values()) {
				for (int itr = 0; itr < numberOfIterations; itr++) {
					QueryExecutor executor = new QueryExecutor(sparkSession, hdfsPath, SourceType.HdfsOrc);
					executor.prepare(tpcSqlId, partitions);
					executor.execute(numberOfIterations);
				}
			}
			sc.close();
			break;
		}
	}

}
