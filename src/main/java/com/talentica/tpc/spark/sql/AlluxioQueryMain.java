/**
 * 
 */
package com.talentica.tpc.spark.sql;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import com.talentica.utility.BenchmarkMode;

/**
 * @author pooshans
 *
 */
public class AlluxioQueryMain {

	public static void main(String[] args) {

		String master = null;
		String appName = null;
		String masterHostAddr = null;
		String masterHostPort = null;
		int numberOfIterations = 0;
		int partitions = 0;// Default 0 will let the spark recognize the number
		// of partition itself otherwise it enforce the
		// number of partition be done by spark.
		int totalTpcSqlId = 22;
		BenchmarkMode benchmarkType = BenchmarkMode.NON_CACHE;
		if (args.length >= 5) {
			master = args[0];
			appName = args[1];
			masterHostAddr = args[2];
			masterHostPort = args[3];
			numberOfIterations = Integer.parseInt(args[4]);
			if (args.length >= 6 && args[5].equalsIgnoreCase("y")) {
				benchmarkType = BenchmarkMode.CACHE;
			}
			if (args.length >= 7) {
				partitions = Integer.parseInt(args[6]);
			}

		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<alluxio-master-addr> ,<alluxio-master-port>,<numberOfIterations>,<optional-cache-(y/n)-default-n>,<optional-partitions-default-0>");
			System.exit(-1);
		}

		System.out.println("Benchmark Mode :: " + benchmarkType.mode());

		switch (benchmarkType) {
		case NON_CACHE:
			for (int itr = 0; itr < numberOfIterations; itr++) {
				for (TPCHSqlId tpcSqlId : TPCHSqlId.values()) {
					SparkConf sparkConf = new SparkConf().setAppName(appName + "-sqlId-" + tpcSqlId).setMaster(master);
					JavaSparkContext sc = new JavaSparkContext(sparkConf);
					SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
					QueryExecutor executor = new QueryExecutor(SourceType.Alluxio, sparkSession, masterHostAddr,
							masterHostPort);
					executor.prepare(tpcSqlId, partitions);
					executor.execute(1);
					sc.close();
				}
			}
			break;
		case CACHE:
			SparkConf sparkConf = new SparkConf().setAppName(appName).setMaster(master);
			JavaSparkContext sc = new JavaSparkContext(sparkConf);
			SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
			for (TPCHSqlId tpcSqlId : TPCHSqlId.values()) {
					QueryExecutor executor = new QueryExecutor(SourceType.Alluxio, sparkSession, masterHostAddr,
							masterHostPort);
					executor.prepare(tpcSqlId, partitions);
					executor.execute(numberOfIterations);
			}
			sc.close();
			break;
		}
	}
}
