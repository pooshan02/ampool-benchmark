/**
 * 
 */
package com.talentica.ampool.bigdatabench.mtables;

import com.talentica.ampool.spark.service.CommonOperation;
import com.talentica.tpc.table.TableEnum;

import io.ampool.client.AmpoolClient;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class OrderItemMtable extends CommonOperation {

	private static final String[] columnNames = new String[] { "ITEM_ID", "ORDER_ID", "GOODS_ID", "GOODS_NUMBER",
			"GOODS_PRICE", "GOODS_AMOUNT" };

	private static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.LONG, BasicTypes.INT,
			BasicTypes.INT, BasicTypes.DOUBLE, BasicTypes.DOUBLE };

	public OrderItemMtable(String[] columnNames, DataType[] columnTypes, String tableName) {
		super(columnNames, columnTypes, tableName);
	}

	public static void main(String[] args) {
		String tableName = TableEnum.OS_ORDER_ITEM.getName();
		OrderItemMtable orderMTable = new OrderItemMtable(columnNames, columnTypes, tableName);
		int batchSize;
		String tableDir;
		if (args.length == 4) {
			batchSize = Integer.parseInt(args[2]);
			tableDir = args[3];
		} else {
			throw new RuntimeException("Please provide the locator , port, batch size and table file path");
		}
		final AmpoolClient client = orderMTable.getClient(args);
		orderMTable.insertData(client, tableDir + "",
				batchSize); // TO DO Provide table path
		orderMTable.getRecords(client, tableName, 10);
	}

}
