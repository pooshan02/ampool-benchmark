/**
 * 
 */
package com.talentica.ampool.bigdatabench.mtables;

import com.talentica.ampool.spark.service.CommonOperation;
import com.talentica.tpc.table.TableEnum;

import io.ampool.client.AmpoolClient;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class OrderMtable extends CommonOperation {

	private static final String[] columnNames = new String[] { "ORDER_ID", "BUYER_ID", "CREATE_DT" };

	private static final DataType[] columnTypes = new DataType[] {BasicTypes.INT, BasicTypes.INT, BasicTypes.DATE };

	public OrderMtable(String[] columnNames, DataType[] columnTypes, String tableName) {
		super(columnNames, columnTypes, tableName);
	}

	public static void main(String[] args) {
		String tableName = TableEnum.OS_ORDER.getName();
		OrderMtable orderMTable = new OrderMtable(columnNames, columnTypes, tableName);
		int batchSize;
		String tableDir;
		if (args.length == 4) {
			batchSize = Integer.parseInt(args[2]);
			tableDir = args[3];
		} else {
			throw new RuntimeException("Please provide the locator , port, batch size and table file path");
		}
		final AmpoolClient client = orderMTable.getClient(args);
		orderMTable.insertData(client, tableDir + "", batchSize);// TO DO provide table path
		orderMTable.getRecords(client, tableName, 10);
	}

}
