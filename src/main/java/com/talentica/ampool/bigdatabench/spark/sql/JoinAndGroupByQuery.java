/**
 * 
 */
package com.talentica.ampool.bigdatabench.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.talentica.utility.QueryCommand;

/**
 * @author pooshans
 *
 */
public class JoinAndGroupByQuery {

	public static void main(String[] args) throws AnalysisException {
		String master = null;
		String appName = null;
		String locatorHost = null;
		String locatorPort = null;
		int numPartitions = -1;

		if (args.length == 5) {
			master = args[0];
			appName = args[1];
			locatorHost = args[2];
			locatorPort = args[3];
			numPartitions = Integer.parseInt(args[4]);
		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<locatorHost> , <locatorPort> , <no-of-repartition {-1} for-default>");
			System.exit(-1);
		}

		/** create SparkSession **/
		SparkSession sparkSession = SparkSession.builder().appName(appName).master(master).getOrCreate();

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset<Row> orderDs = sparkSession.read().format("io.ampool").options(options).load("OS_ORDER");
		Dataset<Row> newOrderDs = (numPartitions != -1) ? orderDs.repartition(numPartitions) : orderDs;

		newOrderDs.createOrReplaceTempView("ORDER");

		Dataset<Row> orderItemDs = sparkSession.read().format("io.ampool").options(options).load("OS_ORDER_ITEM");
		Dataset<Row> neworderItemDs = (numPartitions != -1) ? orderItemDs.repartition(numPartitions) : orderItemDs;
		neworderItemDs.createOrReplaceTempView("ORDER_ITEM");
		String query = QueryCommand.joinAndGroupByQuery;

		Dataset<Row> resultDs = sparkSession.sql(query);
		resultDs.show();
		sparkSession.stop();

	}

}
