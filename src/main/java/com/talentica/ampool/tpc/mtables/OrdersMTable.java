/**
 * 
 */
package com.talentica.ampool.tpc.mtables;

import com.talentica.ampool.spark.service.CommonOperation;
import com.talentica.tpc.table.TableEnum;
import com.talentica.utility.TablePathUtil;

import io.ampool.client.AmpoolClient;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class OrdersMTable extends CommonOperation{

	private static final String[] columnNames = new String[] { "O_ORDERKEY", "O_CUSTKEY", "O_ORDERSTATUS",
			"O_TOTALPRICE", "O_ORDERDATE", "O_ORDERPRIORITY", "O_CLERK", "O_SHIPPRIORITY", "O_COMMENT" };

	private static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.INT, BasicTypes.STRING,
			BasicTypes.DOUBLE, BasicTypes.DATE, BasicTypes.STRING, BasicTypes.STRING, BasicTypes.INT,
			BasicTypes.STRING };

	
	public OrdersMTable(String[] columnNames,DataType[] columnTypes,String tableName){
		super(columnNames,columnTypes,tableName);
	}

	public static void main(String[] args) {
		String tableName = TableEnum.ORDERS.name();
		OrdersMTable orderMTable =  new OrdersMTable(columnNames, columnTypes,tableName );
		int batchSize = 1000;
		if (args.length <= 3) {
			throw new RuntimeException("Please provide the locator , port , table-path and batch size");
		}

		if (args.length >= 3) {
			tpchTablePath = args[2];
		}
		if (args.length == 4) {
			batchSize = Integer.parseInt(args[3]);
		}
		final AmpoolClient client = orderMTable.getClient(args);
		orderMTable.insertData(client, TablePathUtil.ORDERS.getAbsolutePath(tpchTablePath),
				batchSize);
		orderMTable.getRecords(client,tableName,10);
	}

}
