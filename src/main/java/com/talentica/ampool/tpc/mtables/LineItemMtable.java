/**
 * 
 */
package com.talentica.ampool.tpc.mtables;

import com.talentica.ampool.spark.service.CommonOperation;
import com.talentica.tpc.table.TableEnum;
import com.talentica.utility.TablePathUtil;

import io.ampool.client.AmpoolClient;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class LineItemMtable extends CommonOperation {

	private static final String[] columnNames = new String[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER",
			"L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE",
			"L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" };

	private static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.INT, BasicTypes.INT,
			BasicTypes.INT, BasicTypes.DOUBLE, BasicTypes.DOUBLE, BasicTypes.DOUBLE, BasicTypes.DOUBLE,
			BasicTypes.STRING, BasicTypes.STRING, BasicTypes.DATE, BasicTypes.DATE, BasicTypes.DATE, BasicTypes.STRING,
			BasicTypes.STRING, BasicTypes.STRING };

	public LineItemMtable(String[] columnNames, DataType[] columnTypes, String tableName) {
		super(columnNames, columnTypes, tableName);
	}

	public static void main(String[] args) {
		String tableName = TableEnum.LINEITEM.name();
		LineItemMtable orderMTable = new LineItemMtable(columnNames, columnTypes, tableName);
		int batchSize = 1000;
		
		if(args.length <= 3){
			throw new RuntimeException("Please provide the locator , port , table-path and batch size");
		}
		
		if(args.length >= 3){
			tpchTablePath = args[2];
		}
		if(args.length == 4){
			batchSize = Integer.parseInt(args[3]);
		}
		final AmpoolClient client = orderMTable.getClient(args);
		orderMTable.insertData(client, TablePathUtil.LINEITEM.getAbsolutePath(tpchTablePath),batchSize);
		orderMTable.getRecords(client, tableName, 10);
	}

}
