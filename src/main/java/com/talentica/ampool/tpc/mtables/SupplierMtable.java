/**
 * 
 */
package com.talentica.ampool.tpc.mtables;

import com.talentica.ampool.spark.service.CommonOperation;
import com.talentica.tpc.table.TableEnum;
import com.talentica.utility.TablePathUtil;

import io.ampool.client.AmpoolClient;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class SupplierMtable extends CommonOperation {

	private static final String[] columnNames = new String[] { "S_SUPPKEY", "S_NAME", "S_ADDRESS", "S_NATIONKEY",
			"S_PHONE", "S_ACCTBAL", "S_COMMENT" };

	private static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.STRING, BasicTypes.STRING,
			BasicTypes.INT, BasicTypes.STRING, BasicTypes.DOUBLE, BasicTypes.STRING };

	public SupplierMtable(String[] columnNames, DataType[] columnTypes, String tableName) {
		super(columnNames, columnTypes, tableName);
	}

	public static void main(String[] args) {
		String tableName = TableEnum.SUPPLIER.name();
		SupplierMtable orderMTable = new SupplierMtable(columnNames, columnTypes, tableName);
		int batchSize = 1000;
		if (args.length <= 3) {
			throw new RuntimeException("Please provide the locator , port , table-path and batch size");
		}

		if (args.length >= 3) {
			tpchTablePath = args[2];
		}
		if (args.length == 4) {
			batchSize = Integer.parseInt(args[3]);
		}
		final AmpoolClient client = orderMTable.getClient(args);
		orderMTable.insertData(client, TablePathUtil.SUPPLIER.getAbsolutePath(tpchTablePath),
				batchSize);
		orderMTable.getRecords(client, tableName, 10);
	}

}
