package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;


/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class OrderQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("OrderQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_Orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df1.registerTempTable("temp_lineItem");
	    
	    String sql = "select O_ORDERPRIORITY,count(*) as order_count "
	    		+ "from temp_Orders "
	    		+ "where O_ORDERDATE >= date '1993-01-01' "
	    		+ "and O_ORDERDATE < date '1997-10-01' + interval '3' month "
	    		+ "and exists ( select * from temp_lineItem where L_ORDERKEY = O_ORDERKEY and L_COMMITDATE < L_RECEIPTDATE ) "
	    		+ "group by O_ORDERPRIORITY "
	    		+ "order by O_ORDERPRIORITY";
	    
	    List<Row> rows = sqlContext.sql(sql).collectAsList();
	    
	    System.out.println(rows.size());
	    
	    for(Row row : rows){
	    	System.out.println(row.toString());
	    }
	    
	    jsc.stop();
	}

}
