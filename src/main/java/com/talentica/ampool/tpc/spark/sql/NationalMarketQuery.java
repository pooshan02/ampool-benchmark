package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class NationalMarketQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String nation = TableEnum.NATION.name();
		final String region = TableEnum.REGION.name();
		final String part = TableEnum.PART.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("NationalMarketQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
	    df1.registerTempTable("temp_customer");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df3.registerTempTable("temp_supplier");
	    
	    Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df4.registerTempTable("temp_nation");
	    
	    Dataset df5 = sqlContext.read().format("io.ampool").options(options).load(region);
	    df5.registerTempTable("temp_region");
	    
	    Dataset df6 = sqlContext.read().format("io.ampool").options(options).load(part);
	    df6.registerTempTable("temp_part");
	    
	    String sql = "select o_year,sum(case when nation = 'BRAZIL' then volume else 0 end) / sum(volume) as mkt_share "
	    		+ "from ( "
	    		+ "select extract(year from O_ORDERDATE) as o_year,L_EXTENDEDPRICE * (1-L_DISCOUNT) as volume,n2.N_NAME as nation "
	    		+ "from temp_part,temp_supplier,temp_lineItem,temp_orders,temp_customer,temp_nation n1,temp_nation n2,temp_region "
	    		+ "where P_PARTKEY = L_PARTKEY "
	    		+ "and S_SUPPKEY = L_SUPPKEY "
	    		+ "and L_ORDERKEY = O_ORDERKEY "
	    		+ "and O_CUSTKEY = C_CUSTKEY "
	    		+ "and C_NATIONKEY = n1.N_NATIONKEY "
	    		+ "and n1.N_REGIONKEY = R_REGIONKEY "
	    		+ "and R_NAME = 'AMERICA' "
	    		+ "and S_NATIONKEY = n2.N_NATIONKEY "
	    		+ "and O_ORDERDATE between date '1995-01-01' and date '1996-12-31' "
	    		+ "and P_TYPE = 'ECONOMY ANODIZED STEEL' ) as all_nations "
	    		+ "group by o_year "
	    		+ "order by o_year";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
