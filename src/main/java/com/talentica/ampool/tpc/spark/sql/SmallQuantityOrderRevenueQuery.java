package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class SmallQuantityOrderRevenueQuery {

	public static void main(String[] args) {
		final String part = TableEnum.PART.name();
		final String lineItem = TableEnum.LINEITEM.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("SmallQuantityOrderRevenueQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(part);
	    df.registerTempTable("temp_part");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    String sql = "select sum(L_EXTENDEDPRICE) / 7.0 as avg_yearly "
	    		+ "from temp_lineItem,temp_part "
	    		+ "where P_PARTKEY = L_PARTKEY "
	    		+ "and P_BRAND = 'Brand#23' "
	    		+ "and P_CONTAINER = 'MED BOX' "
	    		+ "and L_QUANTITY < ( "
	    		+ "select 0.2 * avg(L_QUANTITY) "
	    		+ "from temp_lineItem "
	    		+ "where L_PARTKEY = P_PARTKEY )";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
