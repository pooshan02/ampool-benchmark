package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class PartSupplierRelationQuery {

	public static void main(String[] args) {
		final String part = TableEnum.PART.name();
		final String partSupp = TableEnum.PARTSUPP.name();
		final String supplier = TableEnum.SUPPLIER.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("PartSupplierRelationQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(part);
	    df.registerTempTable("temp_part");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(partSupp);
	    df1.registerTempTable("temp_partSupp");
	    
	    Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df3.registerTempTable("temp_supplier");
	    
	    String sql = "select P_BRAND,P_TYPE,P_SIZE,count(distinct PS_SUPPKEY) as supplier_cnt "
	    		+ "from temp_partSupp,temp_part "
	    		+ "where P_PARTKEY = PS_PARTKEY "
	    		+ "and P_BRAND <> 'Brand#45' "
	    		+ "and P_TYPE not like 'MEDIUM POLISHED%' "
	    		+ "and P_SIZE in (49, 14, 23, 45, 19, 3, 36, 9) "
	    		+ "and PS_SUPPKEY not in ( "
	    		+ "select S_SUPPKEY "
	    		+ "from temp_supplier "
	    		+ "where S_COMMENT like '%Customer%Complaints%' ) "
	    		+ "group by P_BRAND,P_TYPE,P_SIZE "
	    		+ "order by supplier_cnt desc,P_BRAND,P_TYPE,P_SIZE";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
