/**
 * 
 */
package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class SuppliersWhoKeptOrdersWaitingQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String nation = TableEnum.NATION.name();
		final String region = TableEnum.REGION.name();

		final String locatorHost = args.length > 0 ? args[0] : "localhost";
		final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
		SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077")
				.setAppName("SuppliersWhoKeptOrdersWaitingQuery");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(supplier);
		df1.registerTempTable(supplier.toLowerCase());

		Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
		df2.registerTempTable(lineItem.toLowerCase());

		Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(orders);
		df3.registerTempTable(orders.toLowerCase());

		Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
		df4.registerTempTable(nation.toLowerCase());

		Dataset df5 = sqlContext.read().format("io.ampool").options(options).load(region);
		df5.registerTempTable(region.toLowerCase());

		String sql = "select s_name, count(*) as numwait from supplier, lineitem l1, orders, nation where s_suppkey = l1.l_suppkey "
				+ "and o_orderkey = l1.l_orderkey and o_orderstatus = 'F' and l1.l_receiptdate > l1.l_commitdate and "
				+ "exists ( select * from lineitem l2 where l2.l_orderkey = l1.l_orderkey and l2.l_suppkey <> l1.l_suppkey ) "
				+ "and not exists ( select * from lineitem l3 where l3.l_orderkey = l1.l_orderkey and l3.l_suppkey <> l1.l_suppkey "
				+ "and l3.l_receiptdate > l3.l_commitdate ) and s_nationkey = n_nationkey and n_name = 'SAUDI ARABIA' "
				+ "group by s_name order by numwait desc,s_name";

		sqlContext.sql(sql).show();
	}

}
