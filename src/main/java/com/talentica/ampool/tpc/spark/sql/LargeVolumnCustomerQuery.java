package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class LargeVolumnCustomerQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();
		final String lineItem = TableEnum.LINEITEM.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("LargeVolumnCustomerQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
	    df1.registerTempTable("temp_customer");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    String sql = "select C_NAME,C_CUSTKEY,O_ORDERKEY,O_ORDERDATE,O_TOTALPRICE,sum(L_QUANTITY) "
	    		+ "from temp_customer,temp_orders,temp_lineItem "
	    		+ "where O_ORDERKEY in ( "
	    		+ "select L_ORDERKEY "
	    		+ "from temp_lineItem "
	    		+ "group by L_ORDERKEY having "
	    		+ "sum(L_QUANTITY) > 300 ) "
	    		+ "and C_CUSTKEY = O_CUSTKEY "
	    		+ "and O_ORDERKEY = L_ORDERKEY "
	    		+ "group by C_NAME,C_CUSTKEY,O_ORDERKEY,O_ORDERDATE,O_TOTALPRICE "
	    		+ "order by O_TOTALPRICE desc,O_ORDERDATE";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
