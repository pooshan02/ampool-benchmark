package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class CustomerDistributionQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();

		final String locatorHost = args.length > 0 ? args[0] : "localhost";
		final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
		SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("CustomerDistributionQuery");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
		df.registerTempTable("temp_orders");

		Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
		df1.registerTempTable("temp_customer");

		String sql = "select c_orders.c_count, count(*) as custdist " + "from ( "
				+ "select C_CUSTKEY,count(O_ORDERKEY) as c_count "
				+ "from temp_customer left outer join temp_orders on " + "C_CUSTKEY = O_CUSTKEY "
				+ "and O_COMMENT not like '%special%requests%' " + "group by C_CUSTKEY ) as c_orders "
				+ "group by c_count " + "order by custdist desc,c_count desc";

		sqlContext.sql(sql).show();

		jsc.stop();
	}

}
