/**
 * 
 */
package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class DiscountedRevenueQuery {

	public static void main(String[] args) {
		final String lineItem = TableEnum.LINEITEM.name();
		final String part = TableEnum.PART.name();

		final String locatorHost = args.length > 0 ? args[0] : "localhost";
		final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
		SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("DiscountedRevenueQuery");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
		df1.registerTempTable(lineItem.toLowerCase());

		Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(part);
		df2.registerTempTable(part.toLowerCase());

		String sql = "select sum(l_extendedprice * (1 - l_discount) ) as revenue from lineitem, part where "
				+ "( p_partkey = l_partkey and p_brand = ‘Brand#12’ and p_container in ( ‘SM CASE’, ‘SM BOX’, ‘SM PACK’, ‘SM PKG’) "
				+ "and l_quantity >= 1 and l_quantity <= 11 and p_size between 1 and 5 and l_shipmode in (‘AIR’, ‘AIR REG’) "
				+ "and l_shipinstruct = ‘DELIVER IN PERSON’) or (p_partkey = l_partkey and p_brand = ‘Brand#23’ "
				+ "and p_container in (‘MED BAG’, ‘MED BOX’, ‘MED PKG’, ‘MED PACK’) and l_quantity >= 10 "
				+ "and l_quantity <= 20 and p_size between 1 and 10 and l_shipmode in (‘AIR’, ‘AIR REG’) "
				+ "and l_shipinstruct = ‘DELIVER IN PERSON’) or (p_partkey = l_partkey and p_brand = ‘Brand#34’ "
				+ "and p_container in ( ‘LG CASE’, ‘LG BOX’, ‘LG PACK’, ‘LG PKG’) and l_quantity >= 20 "
				+ "and l_quantity <= 30 and p_size between 1 and 15 and l_shipmode in (‘AIR’, ‘AIR REG’) "
				+ "and l_shipinstruct = ‘DELIVER IN PERSON’)";

		sqlContext.sql(sql).show();
	}

}
