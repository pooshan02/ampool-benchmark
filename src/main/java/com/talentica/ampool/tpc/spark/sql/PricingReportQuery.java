/**
 * 
 */
package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.ampool.spark.service.CommonOperation;
import com.talentica.tpc.table.TableEnum;


/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class PricingReportQuery extends CommonOperation {

	public PricingReportQuery() {
		super();
	}

	public static void main(String[] args) {
		final String tableName = TableEnum.LINEITEM.name();
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("PricingReportQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(tableName);
	    df.registerTempTable("temp_lineItem");
	    String sql = "select L_RETURNFLAG,L_LINESTATUS,sum(L_QUANTITY) as sum_qty,"
	    		+ "sum(L_EXTENDEDPRICE) as sum_base_price,sum(L_EXTENDEDPRICE*(1-L_DISCOUNT)) as sum_disc_price,"
	    		+ "sum(L_EXTENDEDPRICE*(1-L_DISCOUNT)*(1+L_TAX)) as sum_charge,avg(L_QUANTITY) as avg_qty,"
	    		+ "avg(L_EXTENDEDPRICE) as avg_price,avg(L_DISCOUNT) as avg_disc,count(*) as count_order "
	    		+ "from temp_lineItem "
	    		+ "where "
	    		+ "L_SHIPDATE <= date '1998-12-01' - interval '90' day "
	    		+ "group by L_RETURNFLAG,L_LINESTATUS "
	    		+ "order by L_RETURNFLAG,L_LINESTATUS";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
