package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class ForecastRevenueChangeQuery {

	public static void main(String[] args) {
		final String lineItem = TableEnum.LINEITEM.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("ForecastRevenueChangeQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    String sql = "select sum(L_EXTENDEDPRICE*L_DISCOUNT) as revenue "
	    		+ "from temp_lineItem "
	    		+ "where L_SHIPDATE >= date '1994-01-01' "
	    		+ "and L_SHIPDATE < date '1994-01-01' + interval '1' year "
	    		+ "and L_DISCOUNT between 0.06 - 0.01 and 0.06 + 0.01 "
	    		+ "and L_QUANTITY < 24";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
