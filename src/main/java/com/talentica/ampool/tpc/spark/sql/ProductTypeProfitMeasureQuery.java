package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class ProductTypeProfitMeasureQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String part = TableEnum.PART.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String nation = TableEnum.NATION.name();
		final String partSupp = TableEnum.PARTSUPP.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("ProductTypeProfitMeasureQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(part);
	    df1.registerTempTable("temp_part");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df3.registerTempTable("temp_supplier");
	    
	    Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df4.registerTempTable("temp_nation");
	    
	    Dataset df5 = sqlContext.read().format("io.ampool").options(options).load(partSupp);
	    df5.registerTempTable("temp_partSupp");
	    
	    String sql = "select nation,o_year,sum(amount) as sum_profit "
	    		+ "from ( "
	    		+ "select N_NAME as nation,extract(year from O_ORDERDATE) as o_year,"
	    		+ "L_EXTENDEDPRICE * (1 - L_DISCOUNT) - PS_SUPPLYCOST * L_QUANTITY as amount "
	    		+ "from temp_part,temp_supplier,temp_lineItem,temp_partSupp,temp_orders,temp_nation "
	    		+ "where S_SUPPKEY = L_SUPPKEY "
	    		+ "and PS_SUPPKEY = L_SUPPKEY "
	    		+ "and PS_PARTKEY = L_PARTKEY "
	    		+ "and P_PARTKEY = L_PARTKEY "
	    		+ "and O_ORDERKEY = L_ORDERKEY "
	    		+ "and S_NATIONKEY = N_NATIONKEY "
	    		+ "and P_NAME like '%green%' ) as profit "
	    		+ "group by nation,o_year "
	    		+ "order by nation,o_year desc";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
