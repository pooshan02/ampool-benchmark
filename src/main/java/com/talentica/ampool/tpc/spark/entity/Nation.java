/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;

/**
 * @author pooshans
 *
 */
public class Nation implements Serializable {

	private Integer N_NATIONKEY;
	private String N_NAME;
	private Integer N_REGIONKEY;
	private String N_COMMENT;
	
	public Nation(final String[] values){
		int index = 0;
		this.N_NATIONKEY = Integer.valueOf(values[index++]);
		this.N_NAME = values[index++];
		this.N_REGIONKEY = Integer.valueOf(values[index++]);
		this.N_COMMENT = values[index++];
	}
	
	public Integer getN_NATIONKEY() {
		return N_NATIONKEY;
	}
	public void setN_NATIONKEY(Integer n_NATIONKEY) {
		N_NATIONKEY = n_NATIONKEY;
	}
	public String getN_NAME() {
		return N_NAME;
	}
	public void setN_NAME(String n_NAME) {
		N_NAME = n_NAME;
	}
	public Integer getN_REGIONKEY() {
		return N_REGIONKEY;
	}
	public void setN_REGIONKEY(Integer n_REGIONKEY) {
		N_REGIONKEY = n_REGIONKEY;
	}
	public String getN_COMMENT() {
		return N_COMMENT;
	}
	public void setN_COMMENT(String n_COMMENT) {
		N_COMMENT = n_COMMENT;
	}
	
}
