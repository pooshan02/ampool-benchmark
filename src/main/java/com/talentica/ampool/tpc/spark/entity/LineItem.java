/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * @author pooshans
 *
 */
public class LineItem implements Serializable {

	private Integer L_ORDERKEY;
	private Integer L_PARTKEY;
	private Integer L_SUPPKEY;
	private Integer L_LINENUMBER;
	private Double L_QUANTITY;
	private Double L_EXTENDEDPRICE;
	private Double L_DISCOUNT;
	private Double L_TAX;
	private String L_RETURNFLAG;
	private String L_LINESTATUS;
	private Date L_SHIPDATE;
	private Date L_COMMITDATE;
	private Date L_RECEIPTDATE;
	private String L_SHIPINSTRUCT;
	private String L_SHIPMODE;
	private String L_COMMENT;
	
	public LineItem(final String[] values){
		int index = 0;
		this.L_ORDERKEY = Integer.valueOf(values[index++]);
		this.L_PARTKEY = Integer.valueOf(values[index++]);
		this.L_SUPPKEY = Integer.valueOf(values[index++]);
		this.L_LINENUMBER = Integer.valueOf(values[index++]);
		this.L_QUANTITY = Double.valueOf(values[index++]);
		this.L_EXTENDEDPRICE = Double.valueOf(values[index++]);
		this.L_DISCOUNT = Double.valueOf(values[index++]);
		this.L_TAX = Double.valueOf(values[index++]);
		this.L_RETURNFLAG = values[index++];
		this.L_LINESTATUS = values[index++];
		this.L_SHIPDATE = Date.valueOf(values[index++]);
		this.L_COMMITDATE = Date.valueOf(values[index++]);
		this.L_RECEIPTDATE = Date.valueOf(values[index++]);
		this.L_SHIPINSTRUCT = values[index++];
		this.L_SHIPMODE = values[index++];
		this.L_COMMENT = values[index++];
	}
	
	public Integer getL_ORDERKEY() {
		return L_ORDERKEY;
	}
	public void setL_ORDERKEY(Integer l_ORDERKEY) {
		L_ORDERKEY = l_ORDERKEY;
	}
	public Integer getL_PARTKEY() {
		return L_PARTKEY;
	}
	public void setL_PARTKEY(Integer l_PARTKEY) {
		L_PARTKEY = l_PARTKEY;
	}
	public Integer getL_SUPPKEY() {
		return L_SUPPKEY;
	}
	public void setL_SUPPKEY(Integer l_SUPPKEY) {
		L_SUPPKEY = l_SUPPKEY;
	}
	public Integer getL_LINENUMBER() {
		return L_LINENUMBER;
	}
	public void setL_LINENUMBER(Integer l_LINENUMBER) {
		L_LINENUMBER = l_LINENUMBER;
	}
	public Double getL_QUANTITY() {
		return L_QUANTITY;
	}
	public void setL_QUANTITY(Double l_QUANTITY) {
		L_QUANTITY = l_QUANTITY;
	}
	public Double getL_EXTENDEDPRICE() {
		return L_EXTENDEDPRICE;
	}
	public void setL_EXTENDEDPRICE(Double l_EXTENDEDPRICE) {
		L_EXTENDEDPRICE = l_EXTENDEDPRICE;
	}
	public Double getL_DISCOUNT() {
		return L_DISCOUNT;
	}
	public void setL_DISCOUNT(Double l_DISCOUNT) {
		L_DISCOUNT = l_DISCOUNT;
	}
	public Double getL_TAX() {
		return L_TAX;
	}
	public void setL_TAX(Double l_TAX) {
		L_TAX = l_TAX;
	}
	public String getL_RETURNFLAG() {
		return L_RETURNFLAG;
	}
	public void setL_RETURNFLAG(String l_RETURNFLAG) {
		L_RETURNFLAG = l_RETURNFLAG;
	}
	public String getL_LINESTATUS() {
		return L_LINESTATUS;
	}
	public void setL_LINESTATUS(String l_LINESTATUS) {
		L_LINESTATUS = l_LINESTATUS;
	}
	public Date getL_SHIPDATE() {
		return L_SHIPDATE;
	}
	public void setL_SHIPDATE(Date l_SHIPDATE) {
		L_SHIPDATE = l_SHIPDATE;
	}
	public Date getL_COMMITDATE() {
		return L_COMMITDATE;
	}
	public void setL_COMMITDATE(Date l_COMMITDATE) {
		L_COMMITDATE = l_COMMITDATE;
	}
	public Date getL_RECEIPTDATE() {
		return L_RECEIPTDATE;
	}
	public void setL_RECEIPTDATE(Date l_RECEIPTDATE) {
		L_RECEIPTDATE = l_RECEIPTDATE;
	}
	public String getL_SHIPINSTRUCT() {
		return L_SHIPINSTRUCT;
	}
	public void setL_SHIPINSTRUCT(String l_SHIPINSTRUCT) {
		L_SHIPINSTRUCT = l_SHIPINSTRUCT;
	}
	public String getL_SHIPMODE() {
		return L_SHIPMODE;
	}
	public void setL_SHIPMODE(String l_SHIPMODE) {
		L_SHIPMODE = l_SHIPMODE;
	}
	public String getL_COMMENT() {
		return L_COMMENT;
	}
	public void setL_COMMENT(String l_COMMENT) {
		L_COMMENT = l_COMMENT;
	}
	
	
}
