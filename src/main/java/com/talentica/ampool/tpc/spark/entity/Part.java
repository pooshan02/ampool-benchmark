/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;

/**
 * @author pooshans
 *
 */
public class Part implements Serializable {
	private Integer P_PARTKEY;
	private String P_NAME;
	private String P_MFGR;
	private String P_BRAND;
	private String P_TYPE;
	private Integer P_SIZE;
	private String P_CONTAINER;
	private Double P_RETAILPRICE;
	private String P_COMMENT;

	public Part(final String[] values) {
		int index = 0;
		this.P_PARTKEY = Integer.valueOf(values[index++]);
		this.P_NAME = values[index++];
		this.P_MFGR = values[index++];
		this.P_BRAND = values[index++];
		this.P_TYPE = values[index++];
		this.P_SIZE = Integer.valueOf(values[index++]);
		this.P_CONTAINER = values[index++];
		this.P_RETAILPRICE = Double.valueOf(values[index++]);
		this.P_COMMENT = values[index++];
	}

	public Integer getP_PARTKEY() {
		return P_PARTKEY;
	}

	public void setP_PARTKEY(Integer p_PARTKEY) {
		P_PARTKEY = p_PARTKEY;
	}

	public String getP_NAME() {
		return P_NAME;
	}

	public void setP_NAME(String p_NAME) {
		P_NAME = p_NAME;
	}

	public String getP_MFGR() {
		return P_MFGR;
	}

	public void setP_MFGR(String p_MFGR) {
		P_MFGR = p_MFGR;
	}

	public String getP_BRAND() {
		return P_BRAND;
	}

	public void setP_BRAND(String p_BRAND) {
		P_BRAND = p_BRAND;
	}

	public String getP_TYPE() {
		return P_TYPE;
	}

	public void setP_TYPE(String p_TYPE) {
		P_TYPE = p_TYPE;
	}

	public Integer getP_SIZE() {
		return P_SIZE;
	}

	public void setP_SIZE(Integer p_SIZE) {
		P_SIZE = p_SIZE;
	}

	public String getP_CONTAINER() {
		return P_CONTAINER;
	}

	public void setP_CONTAINER(String p_CONTAINER) {
		P_CONTAINER = p_CONTAINER;
	}

	public Double getP_RETAILPRICE() {
		return P_RETAILPRICE;
	}

	public void setP_RETAILPRICE(Double p_RETAILPRICE) {
		P_RETAILPRICE = p_RETAILPRICE;
	}

	public String getP_COMMENT() {
		return P_COMMENT;
	}

	public void setP_COMMENT(String p_COMMENT) {
		P_COMMENT = p_COMMENT;
	}
	

}
