/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;

/**
 * @author pooshans
 *
 */
public class Supplier implements Serializable {

	private Integer S_SUPPKEY;
	private String S_NAME;
	private String S_ADDRESS;
	private Integer S_NATIONKEY;
	private String S_PHONE;
	private Double S_ACCTBAL;
	private String S_COMMENT;
	
	public Supplier(final String[] values){
		int index = 0;
		this.S_SUPPKEY = Integer.valueOf(values[index++]);
		this.S_NAME =values[index++];
		this.S_ADDRESS = values[index++];
		this.S_NATIONKEY = Integer.valueOf(values[index++]);
		this.S_PHONE = values[index++];
		this.S_ACCTBAL = Double.valueOf(values[index++]);
		this.S_COMMENT = values[index++];
		
	}

	public Integer getS_SUPPKEY() {
		return S_SUPPKEY;
	}

	public void setS_SUPPKEY(Integer s_SUPPKEY) {
		S_SUPPKEY = s_SUPPKEY;
	}

	public String getS_NAME() {
		return S_NAME;
	}

	public void setS_NAME(String s_NAME) {
		S_NAME = s_NAME;
	}

	public String getS_ADDRESS() {
		return S_ADDRESS;
	}

	public void setS_ADDRESS(String s_ADDRESS) {
		S_ADDRESS = s_ADDRESS;
	}

	public Integer getS_NATIONKEY() {
		return S_NATIONKEY;
	}

	public void setS_NATIONKEY(Integer s_NATIONKEY) {
		S_NATIONKEY = s_NATIONKEY;
	}

	public String getS_PHONE() {
		return S_PHONE;
	}

	public void setS_PHONE(String s_PHONE) {
		S_PHONE = s_PHONE;
	}

	public Double getS_ACCTBAL() {
		return S_ACCTBAL;
	}

	public void setS_ACCTBAL(Double s_ACCTBAL) {
		S_ACCTBAL = s_ACCTBAL;
	}

	public String getS_COMMENT() {
		return S_COMMENT;
	}

	public void setS_COMMENT(String s_COMMENT) {
		S_COMMENT = s_COMMENT;
	}

}
