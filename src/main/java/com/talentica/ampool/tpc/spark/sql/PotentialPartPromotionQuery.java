/**
 * 
 */
package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class PotentialPartPromotionQuery {

	public static void main(String[] args) {
		final String lineItem = TableEnum.LINEITEM.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String nation = TableEnum.NATION.name();
		final String partSupp = TableEnum.PARTSUPP.name();
		final String part = TableEnum.PART.name();

		final String locatorHost = args.length > 0 ? args[0] : "localhost";
		final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
		SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("PotentialPartPromotionQuery");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(supplier);
		df1.registerTempTable(supplier.toLowerCase());

		Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(nation);
		df2.registerTempTable(nation.toLowerCase());

		Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(partSupp);
		df3.registerTempTable(partSupp.toLowerCase());

		Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(part);
		df4.registerTempTable(part.toLowerCase());

		Dataset df5 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
		df5.registerTempTable(lineItem.toLowerCase());

		Dataset df6 = sqlContext.sql("select p_partkey from part where p_name like 'forest%'");
		df6.registerTempTable("temp1");
		df6.show(4);

		Dataset df7 = sqlContext.sql("select ps_suppkey from partsupp where ps_partkey in (select p_partkey from temp1)");
		df7.registerTempTable("temp2");
		df7.show(4);

		String sql = "select s_name,s_address from supplier,nation " + "where s_suppkey in (select ps_suppkey from temp2) "
				+ "and ps_availqty > (select 0.5 * sum(l_quantity) from lineitem where l_partkey = ps_partkey "
				+ "and l_suppkey = ps_suppkey and l_shipdate >= date('1994-01-01’) "
				+ "and l_shipdate < date('1994-01-01’) + interval ‘1’ year )) and s_nationkey = n_nationkey "
				+ "and n_name = 'CANADA' order by s_name";
		sqlContext.sql(sql).show();
	}

}
