package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class LocalSupplierVolQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String nation = TableEnum.NATION.name();
		final String region = TableEnum.REGION.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("LocalSupplierVolQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
	    df1.registerTempTable("temp_customer");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df3.registerTempTable("temp_supplier");
	    
	    Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df4.registerTempTable("temp_nation");
	    
	    Dataset df5 = sqlContext.read().format("io.ampool").options(options).load(region);
	    df5.registerTempTable("temp_region");
	    
	    String sql = "select N_NAME,sum(L_EXTENDEDPRICE * (1 - L_DISCOUNT)) as revenue "
	    		+ "from temp_customer,temp_orders,temp_lineItem,temp_supplier,temp_nation,temp_region "
	    		+ "where C_CUSTKEY = O_CUSTKEY "
	    		+ "and L_ORDERKEY = O_ORDERKEY "
	    		+ "and L_SUPPKEY = S_SUPPKEY "
	    		+ "and C_NATIONKEY = S_NATIONKEY "
	    		+ "and S_NATIONKEY = N_NATIONKEY "
	    		+ "and N_REGIONKEY = R_REGIONKEY "
	    		+ "and R_NAME = 'ASIA' "
	    		+ "and O_ORDERDATE >= date '1994-01-01' "
	    		+ "and O_ORDERDATE < date '1994-01-01' + interval '1' year "
	    		+ "group by N_NAME "
	    		+ "order by revenue desc";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
