/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * @author pooshans
 *
 */
public class Orders implements Serializable {

	private Integer O_ORDERKEY;
	private Integer O_CUSTKEY;
	private String O_ORDERSTATUS;
	private Double O_TOTALPRICE;
	private Date O_ORDERDATE;
	private String O_ORDERPRIORITY;
	private String O_CLERK;
	private Integer O_SHIPPRIORITY;
	private String O_COMMENT;
	
	public Orders(final String[] values){
		int index = 0;
		this.O_ORDERKEY = Integer.valueOf(values[index++]);
		this.O_CUSTKEY = Integer.valueOf(values[index++]);
		this.O_ORDERSTATUS = values[index++];
		this.O_TOTALPRICE = Double.valueOf(values[index++]);
		this.O_ORDERDATE = Date.valueOf(values[index++]);
		this.O_ORDERPRIORITY = values[index++];
		this.O_CLERK = values[index++];
		this.O_SHIPPRIORITY = Integer.valueOf(values[index++]);
		this.O_COMMENT = values[index++];
	}
	
	public Integer getO_ORDERKEY() {
		return O_ORDERKEY;
	}
	public void setO_ORDERKEY(Integer o_ORDERKEY) {
		O_ORDERKEY = o_ORDERKEY;
	}
	public Integer getO_CUSTKEY() {
		return O_CUSTKEY;
	}
	public void setO_CUSTKEY(Integer o_CUSTKEY) {
		O_CUSTKEY = o_CUSTKEY;
	}
	public String getO_ORDERSTATUS() {
		return O_ORDERSTATUS;
	}
	public void setO_ORDERSTATUS(String o_ORDERSTATUS) {
		O_ORDERSTATUS = o_ORDERSTATUS;
	}
	public Double getO_TOTALPRICE() {
		return O_TOTALPRICE;
	}
	public void setO_TOTALPRICE(Double o_TOTALPRICE) {
		O_TOTALPRICE = o_TOTALPRICE;
	}
	public Date getO_ORDERDATE() {
		return O_ORDERDATE;
	}
	public void setO_ORDERDATE(Date o_ORDERDATE) {
		O_ORDERDATE = o_ORDERDATE;
	}
	public String getO_ORDERPRIORITY() {
		return O_ORDERPRIORITY;
	}
	public void setO_ORDERPRIORITY(String o_ORDERPRIORITY) {
		O_ORDERPRIORITY = o_ORDERPRIORITY;
	}
	public String getO_CLERK() {
		return O_CLERK;
	}
	public void setO_CLERK(String o_CLERK) {
		O_CLERK = o_CLERK;
	}
	public Integer getO_SHIPPRIORITY() {
		return O_SHIPPRIORITY;
	}
	public void setO_SHIPPRIORITY(Integer o_SHIPPRIORITY) {
		O_SHIPPRIORITY = o_SHIPPRIORITY;
	}
	public String getO_COMMENT() {
		return O_COMMENT;
	}
	public void setO_COMMENT(String o_COMMENT) {
		O_COMMENT = o_COMMENT;
	}
	
}
