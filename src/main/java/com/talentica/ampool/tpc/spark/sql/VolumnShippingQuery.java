package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class VolumnShippingQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String nation = TableEnum.NATION.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("VolumnShippingQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
	    df1.registerTempTable("temp_customer");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df3.registerTempTable("temp_supplier");
	    
	    Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df4.registerTempTable("temp_nation");
	    
	    String subSql = "select n1.N_NAME as supp_nation,n2.N_NAME as cust_nation,extract(year from L_SHIPDATE) as l_year,"
	    		+ "L_EXTENDEDPRICE * (1 - L_DISCOUNT) as volume "
	    		+ "from temp_supplier,temp_lineItem,temp_orders,temp_customer,temp_nation n1,temp_nation n2 "
	    		+ "where S_SUPPKEY = L_SUPPKEY "
	    		+ "and O_ORDERKEY = L_ORDERKEY "
	    		+ "and C_CUSTKEY = O_CUSTKEY "
	    		+ "and S_NATIONKEY = n1.N_NATIONKEY "
	    		+ "and C_NATIONKEY = n2.N_NATIONKEY "
	    		+ "and ( (n1.N_NAME = 'FRANCE' and n2.N_NAME = 'GERMANY') "
	    		+ "or (n1.N_NAME = 'GERMANY' and n2.N_NAME = 'FRANCE')) "
	    		+ "and L_SHIPDATE between date '1995-01-01' and date '1996-12-31' ";
	    
	    Dataset df5 = sqlContext.sql(subSql);
	    df5.registerTempTable("shipping");
	    
	    String sql = "select supp_nation,cust_nation,l_year, sum(volume) as revenue "
	    		+ "from " +/*( " 
	    		+ "select n1.N_NAME as supp_nation,n2.N_NAME as cust_nation,extract(year from L_SHIPDATE) as l_year,"
	    		+ "L_EXTENDEDPRICE * (1 - L_DISCOUNT) as volume "
	    		+ "from temp_supplier,temp_lineItem,temp_orders,temp_customer,temp_nation n1,temp_nation n2 "
	    		+ "where S_SUPPKEY = L_SUPPKEY "
	    		+ "and O_ORDERKEY = L_ORDERKEY "
	    		+ "and C_CUSTKEY = O_CUSTKEY "
	    		+ "and S_NATIONKEY = n1.N_NATIONKEY "
	    		+ "and C_NATIONKEY = n2.N_NATIONKEY "
	    		+ "and ( (n1.N_NAME = 'FRANCE' and n2.N_NAME = 'GERMANY') "
	    		+ "or (n1.N_NAME = 'GERMANY' and n2.N_NAME = 'FRANCE')) "
	    		+ "and L_SHIPDATE between date '1995-01-01' and date '1996-12-31' ) "
	    		+ "as*/ "shipping "
	    		+ "group by supp_nation,cust_nation,l_year "
	    		+ "order by supp_nation,cust_nation,l_year";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
