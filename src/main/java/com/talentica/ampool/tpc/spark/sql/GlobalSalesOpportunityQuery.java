/**
 * 
 */
package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class GlobalSalesOpportunityQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();

		final String locatorHost = args.length > 0 ? args[0] : "localhost";
		final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
		SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("GlobalSalesOpportunityQuery");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
		df.registerTempTable(orders.toLowerCase());

		Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
		df1.registerTempTable(customer.toLowerCase());

		String sql = "select cntrycode, count(*) as numcust, sum(c_acctbal) as totacctbal from ( select substring(c_phone from 1 for 2) as "
				+ "cntrycode,c_acctbal from customer where   substring(c_phone from 1 for 2) in ('13', '31', '23', '29', '30', '18', '17') and c_acctbal > "
				+ "( select avg(c_acctbal) from customer where c_acctbal > 0.00 and substring(c_phone from 1 for 2) in ('13', '31', '23', '29', '30', '18', '17') ) "
				+ "and not exists ( select * from orders where o_custkey = c_custkey ) ) as custsale group by cntrycode order by cntrycode";

		sqlContext.sql(sql).show();
	}

}
