package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class ShippingModeAndOrderPriorityQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String lineItem = TableEnum.LINEITEM.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("ShippingModeAndOrderPriorityQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	   
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    String sql = "select L_SHIPMODE,sum(case when O_ORDERPRIORITY ='1-URGENT' or O_ORDERPRIORITY ='2-HIGH' then 1 else 0 end)"
	    		+ " as high_line_count,"
	    		+ "sum(case when O_ORDERPRIORITY <> '1-URGENT' and O_ORDERPRIORITY <> '2-HIGH' then 1 else 0 end) "
	    		+ "as low_line_count "
	    		+ "from temp_orders,temp_lineItem "
	    		+ "where O_ORDERKEY = L_ORDERKEY "
	    		+ "and L_SHIPMODE in ('MAIL', 'SHIP') "
	    		+ "and L_COMMITDATE < L_RECEIPTDATE "
	    		+ "and L_SHIPDATE < L_COMMITDATE "
	    		+ "and L_RECEIPTDATE >= date '1994-01-01' "
	    		+ "and L_RECEIPTDATE < date '1994-01-01' + interval '1' year "
	    		+ "group by L_SHIPMODE "
	    		+ "order by L_SHIPMODE";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
