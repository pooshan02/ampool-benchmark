package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class MinimumCostSupplierQuery {

	public static void main(String[] args) {
		final String part = TableEnum.PART.name();
		final String supplier = TableEnum.SUPPLIER.name();
		final String partSupp = TableEnum.PARTSUPP.name();
		final String nation = TableEnum.NATION.name();
		final String region = TableEnum.REGION.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("MinimumCostSupplierQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(part);
	    df1.registerTempTable("temp_part");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df2.registerTempTable("temp_supplier");
	    
	    Dataset df3 = sqlContext.read().format("io.ampool").options(options).load(partSupp);
	    df3.registerTempTable("temp_partSupp");
	    
	    Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df4.registerTempTable("temp_nation");
	    
	    Dataset df5 = sqlContext.read().format("io.ampool").options(options).load(region);
	    df5.registerTempTable("temp_region");
	    
	    String sql = "select S_ACCTBAL,S_NAME,N_NAME,P_PARTKEY,P_MFGR,S_ADDRESS,S_PHONE,S_COMMENT "
	    		+ "from temp_part,temp_supplier,temp_partSupp,temp_nation,temp_region "
	    		+ "where P_PARTKEY = PS_PARTKEY "
	    		+ "and S_SUPPKEY = PS_SUPPKEY "
	    		+ "and P_SIZE = 15 "
	    		+ "and P_TYPE like '%BRASS' "
	    		+ "and S_NATIONKEY = N_NATIONKEY "
	    		+ "and N_REGIONKEY = R_REGIONKEY "
	    		+ "and R_NAME = 'EUROPE' "
	    		+ "and PS_SUPPLYCOST = ( "
	    		+ "select min(PS_SUPPLYCOST) "
	    		+ "from temp_partSupp, temp_supplier,temp_nation, temp_region "
	    		+ "where P_PARTKEY = PS_PARTKEY "
	    		+ "and S_SUPPKEY = PS_SUPPKEY "
	    		+ "and S_NATIONKEY = N_NATIONKEY "
	    		+ "and N_REGIONKEY = R_REGIONKEY "
	    		+ "and R_NAME = 'EUROPE' ) "
	    		+ "order by S_ACCTBAL desc,N_NAME,S_NAME,P_PARTKEY";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
