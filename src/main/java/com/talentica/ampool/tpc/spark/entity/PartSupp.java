/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;

/**
 * @author pooshans
 *
 */
public class PartSupp implements Serializable {

	private Integer PS_PARTKEY;
	private Integer PS_SUPPKEY;
	private Integer PS_AVAILQTY;
	private Double PS_SUPPLYCOST;
	private String PS_COMMENT;
	
	public PartSupp(final String[] values){
		int index = 0;
		this.PS_PARTKEY = Integer.valueOf(values[index++]);
		this.PS_SUPPKEY = Integer.valueOf(values[index++]);
		this.PS_AVAILQTY = Integer.valueOf(values[index++]);
		this.PS_SUPPLYCOST = Double.valueOf(values[index++]);
		this.PS_COMMENT = values[index++];
	}
	
	public Integer getPS_PARTKEY() {
		return PS_PARTKEY;
	}
	public void setPS_PARTKEY(Integer pS_PARTKEY) {
		PS_PARTKEY = pS_PARTKEY;
	}
	public Integer getPS_SUPPKEY() {
		return PS_SUPPKEY;
	}
	public void setPS_SUPPKEY(Integer pS_SUPPKEY) {
		PS_SUPPKEY = pS_SUPPKEY;
	}
	public Integer getPS_AVAILQTY() {
		return PS_AVAILQTY;
	}
	public void setPS_AVAILQTY(Integer pS_AVAILQTY) {
		PS_AVAILQTY = pS_AVAILQTY;
	}
	public Double getPS_SUPPLYCOST() {
		return PS_SUPPLYCOST;
	}
	public void setPS_SUPPLYCOST(Double pS_SUPPLYCOST) {
		PS_SUPPLYCOST = pS_SUPPLYCOST;
	}
	public String getPS_COMMENT() {
		return PS_COMMENT;
	}
	public void setPS_COMMENT(String pS_COMMENT) {
		PS_COMMENT = pS_COMMENT;
	}
	
}
