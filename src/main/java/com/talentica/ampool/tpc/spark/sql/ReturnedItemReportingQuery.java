package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class ReturnedItemReportingQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String customer = TableEnum.CUSTOMER.name();
		final String lineItem = TableEnum.LINEITEM.name();
		final String nation = TableEnum.NATION.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("ReturnedItemReportingQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(customer);
	    df1.registerTempTable("temp_customer");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df2.registerTempTable("temp_lineItem");
	    
	    Dataset df4 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df4.registerTempTable("temp_nation");

	    String sql = "select C_CUSTKEY,C_NAME,sum(L_EXTENDEDPRICE * (1 - L_DISCOUNT)) as revenue,C_ACCTBAL,"
	    		+ "N_NAME,C_ADDRESS,C_PHONE,C_COMMENT "
	    		+ "from temp_customer,temp_orders,temp_lineItem,temp_nation "
	    		+ "where C_CUSTKEY = O_CUSTKEY "
	    		+ "and L_ORDERKEY = O_ORDERKEY "
	    		+ "and O_ORDERDATE >= date '1993-10-01' "
	    		+ "and O_ORDERDATE < date '1993-10-01' + interval '3' month "
	    		+ "and L_RETURNFLAG = 'R' "
	    		+ "and C_NATIONKEY = N_NATIONKEY "
	    		+ "group by C_CUSTKEY,C_NAME,C_ACCTBAL,C_PHONE,N_NAME,C_ADDRESS,C_COMMENT "
	    		+ "order by revenue desc";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
