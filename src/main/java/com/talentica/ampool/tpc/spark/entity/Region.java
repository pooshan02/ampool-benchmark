/**
 * 
 */
package com.talentica.ampool.tpc.spark.entity;

import java.io.Serializable;

/**
 * @author pooshans
 *
 */
public class Region implements Serializable {
	
	private Integer R_REGIONKEY;
	private String R_NAME;
	private String R_COMMENT;
	
	public Region(final String[] values){
		int index = 0;
		this.R_REGIONKEY = Integer.valueOf(values[index++]);
		this.R_NAME =values[index++];
		this.R_COMMENT = values[index++];
	}
	
	public Integer getR_REGIONKEY() {
		return R_REGIONKEY;
	}
	public void setR_REGIONKEY(Integer r_REGIONKEY) {
		R_REGIONKEY = r_REGIONKEY;
	}
	public String getR_NAME() {
		return R_NAME;
	}
	public void setR_NAME(String r_NAME) {
		R_NAME = r_NAME;
	}
	public String getR_COMMENT() {
		return R_COMMENT;
	}
	public void setR_COMMENT(String r_COMMENT) {
		R_COMMENT = r_COMMENT;
	}

}
