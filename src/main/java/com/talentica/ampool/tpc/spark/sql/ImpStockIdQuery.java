package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class ImpStockIdQuery {

	public static void main(String[] args) {
		final String partSupp = TableEnum.PARTSUPP.name();
		final String nation = TableEnum.NATION.name();
		final String supplier = TableEnum.SUPPLIER.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("ImpStockIdQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(partSupp);
	    df.registerTempTable("temp_partSupp");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(nation);
	    df1.registerTempTable("temp_nation");
	    
	    Dataset df2 = sqlContext.read().format("io.ampool").options(options).load(supplier);
	    df2.registerTempTable("temp_supplier");
	    
	    /*String subQuery = "select sum(PS_SUPPLYCOST * PS_AVAILQTY) * 0.0001 "
	    		+ "from temp_partSupp,temp_supplier,temp_nation "
	    		+ "where PS_SUPPKEY = S_SUPPKEY and S_NATIONKEY = N_NATIONKEY and N_NAME = 'GERMANY'";
	    
	    String val = sqlContext.sql(subQuery).first().get(0).toString();
	    
	    System.out.println("************************************");
	    System.out.println("val : " + val);
	    System.out.println("************************************");*/
	    String sql = "select PS_PARTKEY,sum(PS_SUPPLYCOST * PS_AVAILQTY) as value "
	    		+ "from temp_partSupp,temp_supplier,temp_nation "
	    		+ "where PS_SUPPKEY = S_SUPPKEY and S_NATIONKEY = N_NATIONKEY and N_NAME = 'GERMANY' "
	    		+ "group by PS_PARTKEY having sum(PS_SUPPLYCOST * PS_AVAILQTY) > "
	    		+ "( select sum(PS_SUPPLYCOST * PS_AVAILQTY) * 0.0001 "
	    		+ "from temp_partSupp,temp_supplier,temp_nation "
	    		+ "where PS_SUPPKEY = S_SUPPKEY and S_NATIONKEY = N_NATIONKEY and N_NAME = 'GERMANY' ) "
	    		+ " order by value desc";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
