package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

import com.talentica.tpc.table.TableEnum;
/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class PromotionEffectQuery {

	public static void main(String[] args) {
		final String lineItem = TableEnum.LINEITEM.name();
		final String part = TableEnum.PART.name();

		String master = null;
		String appName = null;
		String locatorHost = null;
		String locatorPort = null;
		String showTable = "N";
		int numberOfIterations = 0;
		int repartition = -1;

		if (args.length >= 6) {
			master = args[0];
			appName = args[1];
			locatorHost = args[2];
			locatorPort = args[3];// master port {9000} }
			showTable = args[4]; // It can be either "Y" or "N"
			numberOfIterations = Integer.parseInt(args[5]);
			if(args.length == 7)repartition = Integer.parseInt(args[6]);

		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<locatorHost> , <locatorPort> ,<showTable-Y/N>,<numberOfIterations>,<repartition-optional>");
			System.exit(-1);
		}

		SQLContext sqlContext = SparkSession.builder().appName(appName).master(master).getOrCreate().sqlContext();

		Map<String, String> options = new HashMap<>(3);
		options.put("ampool.locator.host", locatorHost);
		options.put("ampool.locator.port", String.valueOf(locatorPort));

		Dataset<Row> df = sqlContext.read().format("io.ampool").options(options).load(part);
		df.createOrReplaceTempView("temp_part");

		Dataset<Row> df2 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
		df2.createOrReplaceTempView("temp_lineItem");
		long startTime = 0l;
		long totalTime = 0l;
		int lterations = numberOfIterations;
		String sql = "select 100.00 * sum("
				+ "case when P_TYPE like 'PROMO%' then L_EXTENDEDPRICE*(1-L_DISCOUNT) else 0 end)"
				+ " / sum(L_EXTENDEDPRICE * (1 - L_DISCOUNT)) as promo_revenue " + "from temp_lineItem,temp_part "
				+ "where L_PARTKEY = P_PARTKEY " + "and L_SHIPDATE >= date '1995-09-01' "
				+ "and L_SHIPDATE < date '1995-09-01' + interval '1' month";

		while (lterations != 0) {
			startTime = System.currentTimeMillis();
			Dataset<Row> ds = sqlContext.sql(sql);
			if (showTable.equalsIgnoreCase("Y")) {
				if (repartition == -1) {
					ds.show();
				} else {
					Dataset<Row> newDs = ds.repartition(repartition);
					newDs.show();
				}
			}
			long currentTime = System.currentTimeMillis();
			totalTime = totalTime + (currentTime - startTime);
			lterations--;
		}

		System.out.println("Avg. time(ms) taken :: " + (totalTime / numberOfIterations));
		sqlContext.sparkSession().close();
	}

}
