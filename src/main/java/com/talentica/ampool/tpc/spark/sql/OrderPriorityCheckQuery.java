package com.talentica.ampool.tpc.spark.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

/**
 * @deprecated This class is deprecated. Use {@link #AmpoolQueryMain}.
 *
 */
public class OrderPriorityCheckQuery {

	public static void main(String[] args) {
		final String orders = TableEnum.ORDERS.name();
		final String lineItem = TableEnum.LINEITEM.name();
		
		final String locatorHost = args.length > 0 ? args[0] : "localhost";
	    final int locatorPort = args.length > 1 ? Integer.valueOf(args[1]) : 10334;
	    SparkConf conf = new SparkConf().setMaster("spark://ampool1:7077").setAppName("OrderPriorityCheckQuery");
	    JavaSparkContext jsc = new JavaSparkContext(conf);
	    SQLContext sqlContext = new SQLContext(jsc);
	    
	    Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset df = sqlContext.read().format("io.ampool").options(options).load(orders);
	    df.registerTempTable("temp_orders");
	    
	    Dataset df1 = sqlContext.read().format("io.ampool").options(options).load(lineItem);
	    df1.registerTempTable("temp_lineItem");
	    
	    String sql = "select O_ORDERPRIORITY,count(*) as order_count "
	    		+ "from temp_orders "
	    		+ "where O_ORDERDATE >= date '1993-07-01' "
	    		+ "and O_ORDERDATE < date '1993-07-01' + interval '3' month "
	    		+ "and exists ( "
	    		+ "select * "
	    		+ "from temp_lineItem "
	    		+ "where L_ORDERKEY = O_ORDERKEY "
	    		+ "and L_COMMITDATE < L_RECEIPTDATE ) "
	    		+ "group by O_ORDERPRIORITY "
	    		+ "order by O_ORDERPRIORITY";
	    
	    sqlContext.sql(sql).show();
	    
	    jsc.stop();
	}

}
