/**
 * 
 */
package com.talentica.ampool.spark.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.talentica.utility.TableFileReader;

import io.ampool.client.AmpoolClient;
import io.ampool.conf.Constants;
import io.ampool.monarch.table.Cell;
import io.ampool.monarch.table.Get;
import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.MTableDescriptor;
import io.ampool.monarch.table.Put;
import io.ampool.monarch.table.Row;
import io.ampool.monarch.table.Schema;
import io.ampool.monarch.table.exceptions.RowKeyOutOfRangeException;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class CommonOperation {

	private String[] columnNames;
	private DataType[] columnTypes;
	private String tableName;
	protected static String locatorHost;
	protected static int locatorPort;
	protected static String tpchTablePath = null;

	public CommonOperation() {
	}

	public CommonOperation(String[] columnNames, DataType[] columnTypes, String tableName) {
		this.columnNames = columnNames;
		this.columnTypes = columnTypes;
		this.tableName = tableName;
	}

	protected AmpoolClient getClient(String[] args) {
		locatorHost = "localhost";
		locatorPort = 10334;
		if (args.length == 4) {
			locatorHost = args[0];
			locatorPort = Integer.parseInt(args[1]);
		}
		Properties props = new Properties();
		props.setProperty(Constants.MClientCacheconfig.MONARCH_CLIENT_LOG, "/tmp/" + tableName + ".log");
		return new AmpoolClient(locatorHost, locatorPort, props);
	}

	public String getLocatorHost() {
		return locatorHost;
	}

	public int getLocatorPort() {
		return locatorPort;
	}

	private static String keyPrefix = "rowKey_";

	protected void insertData(final AmpoolClient client, String tableFilePath, int batchSize) {
		TableFileReader fileReader = new TableFileReader(tableFilePath,"\\|");
		boolean isBatchOperation = (batchSize == -1) ? false : true;
		MTableDescriptor td = new MTableDescriptor();
		td.setSchema(new Schema(columnNames, columnTypes));
		td.setTotalNumOfSplits(20);
		int startKey = 0;
		int stopKey = 1000;
		/*
		 * td.setStartStopRangeKey(Bytes.toBytes(startKey),
		 * Bytes.toBytes(stopKey));
		 */
		td.setStartStopRangeKey(startKey, stopKey);
		final MTable mTable = client.getAdmin().createMTable(tableName, td);
		List<Put> puts = null;
		if (isBatchOperation) {
			puts = new ArrayList<Put>(batchSize);
		}
		/** putting the data into table **/
		int key = 0;
		int counter = 0;
		int batchId = 0;
		while (fileReader.hasNext()) {
			String[] values = fileReader.next();
			Put put = getPut(key, values);
			if (isBatchOperation) {
				puts.add(put);
				counter++;
				if (counter == batchSize) {
					try {
						mTable.put(puts);
					} catch (RowKeyOutOfRangeException rkoex) {
						rkoex.printStackTrace();
						throw rkoex;
					}
					puts.clear();
					counter = 0;
					batchId++;
					int totalRows = batchId * batchSize;
					System.out.println("Batch# [" + batchId + "] and total row  inserted :: " + totalRows);
				}
			} else {
				counter++;
				mTable.put(put);
				if (counter > 50000 && counter % 50000 == 0) {
					System.out.println(counter);
				}
			}
			key++;
		}
		if (isBatchOperation && puts != null && !puts.isEmpty()) {
			mTable.put(puts);
		}
	}

	private Put getPut(int key, String[] values) {
		Put put = new Put(keyPrefix + key);
		for (int j = 0; j < values.length; j++) {
			BasicTypes dataType = (BasicTypes) columnTypes[j];
			switch (dataType) {
			case INT:
				int valueInt = Integer.valueOf(values[j]);
				put.addColumn(columnNames[j], valueInt);
				break;
			case DOUBLE:
				double valueDobule = Double.valueOf(values[j]);
				put.addColumn(columnNames[j], valueDobule);
				break;
			case DATE:
				Date valueDate = Date.valueOf(values[j]);
				put.addColumn(columnNames[j], valueDate);
				break;
			case SHORT:
				short valueShort = Short.valueOf(values[j]);
				put.addColumn(columnNames[j], valueShort);
				break;
			case FLOAT:
				float valueFloat = Float.valueOf(values[j]);
				put.addColumn(columnNames[j], valueFloat);
				break;
			case LONG:
				long valueLong = Long.valueOf(values[j]);
				put.addColumn(columnNames[j], valueLong);
				break;
			case BYTE:
				byte valueByte = Byte.valueOf(values[j]);
				put.addColumn(columnNames[j], valueByte);
				break;
			case STRING:
				String value = String.valueOf(values[j]);
				put.addColumn(columnNames[j], value);
				break;
			default:
				System.err.println("Invalid data type");
				System.exit(-1);
			}
		}
		return put;
	}

	protected void getRecords(final AmpoolClient client, String tableName, int numberOfRecords) {
		Get get;
		Row row;
		List<Cell> cells;
		System.out.println("Retrieving all columns:");
		final MTable mTable = client.getMTable(tableName);
		for (int i = 0; i < numberOfRecords; i++) {
			get = new Get("rowKey_" + i);
			row = mTable.get(get);
			cells = row.getCells();
			for (int j = 0; j < cells.size(); j++) {
				System.out.printf("| %6s  ", cells.get(j).getColumnValue());
			}
			System.out.println("|");
		}
		System.out.println();
	}

}
