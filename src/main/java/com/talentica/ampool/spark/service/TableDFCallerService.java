/**
 * 
 */
package com.talentica.ampool.spark.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author pooshans
 *
 */
public class TableDFCallerService {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		String tableName = "";
		String batchSize = "";
		if (args.length == 1) {
			tableName = args[0];
		} else if(args.length == 2){
			tableName = args[0];
			batchSize = args[1];
		}else {
			System.err.println(
					"Please provide the table name. Available tables are {part, supplier, partsupp, customer, orders, lineitem, nation, region}");
			System.exit(-1);
		}

		Method method;
		Class<?> cls;
		final String[] params = new String[]{"localhost","10334",batchSize};
		switch (tableName.toLowerCase()) {
		case "part":
			cls = Class.forName("com.talentica.ampool.spark.PartDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "supplier":
			cls = Class.forName("com.talentica.ampool.spark.SupplierDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "partsupp":
			cls = Class.forName("com.talentica.ampool.spark.PartSuppDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "customer":
			cls = Class.forName("com.talentica.ampool.spark.CustomerDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "orders":
			cls = Class.forName("com.talentica.ampool.spark.OrdersDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "lineitem":
			cls = Class.forName("com.talentica.ampool.spark.LineItemDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "nation":
			cls = Class.forName("com.talentica.ampool.spark.NationDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		case "region":
			cls = Class.forName("com.talentica.ampool.spark.RegionDFMain");
			method = cls.getMethod("main", String[].class);
			method.invoke(null, (Object) params);
			break;
		default:
			System.err.println("Invalid table name");
		}

	}

}
