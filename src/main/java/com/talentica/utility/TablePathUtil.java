package com.talentica.utility;

public enum TablePathUtil {

	/** The TPC-H Tables **/
		PART("part.tbl"), SUPPLIER("supplier.tbl"), PARTSUPP("partsupp.tbl"), CUSTOMER("customer.tbl"), ORDERS("orders.tbl"), LINEITEM("lineitem.tbl"), NATION("nation.tbl"), REGION("region.tbl");

	String DELIMITER = "\\|";

	//"hdfs://ampool1:9000/home/talentica/softwares/2.17.2/dbgen/gendata";
	//"/home/talentica/softwares/2.17.2/dbgen/gendata";
	private String tableName;  

	private TablePathUtil(String tableName) {
		this.tableName = tableName;
	}

	public String table() {
		return tableName;
	}

	public String getAbsolutePath(String parentPath) {
		if (parentPath.charAt(parentPath.length() - 1) != '/') {
			return parentPath + "/" + this.table();
		} else {
			throw new RuntimeException("Path should be without slash ('/') at the end");
		}
	}
}
