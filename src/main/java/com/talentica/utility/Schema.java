/**
 * 
 */
package com.talentica.utility;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

/**
 * @author pooshans
 *
 */
public class Schema {

	private Schema() {
	}

	public static StructType getOrderItemTableSchema() {
		StructField[] fields = new StructField[6];
		fields[0] = DataTypes.createStructField("ITEM_ID", DataTypes.IntegerType, true, Metadata.empty());
		fields[1] = DataTypes.createStructField("ORDER_ID", DataTypes.LongType, true, Metadata.empty());
		fields[2] = DataTypes.createStructField("GOODS_ID", DataTypes.IntegerType, true, Metadata.empty());
		fields[3] = DataTypes.createStructField("GOODS_NUMBER", DataTypes.IntegerType, true, Metadata.empty());
		fields[4] = DataTypes.createStructField("GOODS_PRICE", DataTypes.DoubleType, true, Metadata.empty());
		fields[5] = DataTypes.createStructField("GOODS_AMOUNT", DataTypes.DoubleType, true, Metadata.empty());
		StructType schema = DataTypes.createStructType(fields);
		return schema;
	}

	public static StructType getOrderTableSchema() {
		StructField[] fields = new StructField[3];
		fields[0] = DataTypes.createStructField("ORDER_ID", DataTypes.IntegerType, true, Metadata.empty());
		fields[1] = DataTypes.createStructField("BUYER_ID", DataTypes.IntegerType, true, Metadata.empty());
		fields[2] = DataTypes.createStructField("CREATE_DT", DataTypes.DateType, true, Metadata.empty());
		StructType schema = DataTypes.createStructType(fields);
		return schema;
	}

}
