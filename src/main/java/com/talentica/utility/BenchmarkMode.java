/**
 * 
 */
package com.talentica.utility;

/**
 * @author pooshans
 *
 */
public enum BenchmarkMode {
	CACHE("cache"), NON_CACHE("non_cache");

	private String mode;

	private BenchmarkMode(String mode) {
		this.mode = mode;
	}

	public String mode() {
		return mode;
	}

}
