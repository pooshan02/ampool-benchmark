/**
 * 
 */
package com.talentica.utility;

/**
 * @author pooshans
 *
 */
public interface QueryCommand {

	/** BigDataBench query command **/
	String joinAndGroupByQuery = "select ORDER.BUYER_ID, sum(ORDER_ITEM.GOODS_AMOUNT) "
			+ "as total from ORDER_ITEM join ORDER on ORDER_ITEM.ORDER_ID = ORDER.ORDER_ID "
			+ "group by ORDER.BUYER_ID";

	String groupByQuery = "select GOODS_ID, sum(GOODS_NUMBER) from ORDER_ITEM group by GOODS_ID";

	String filterQuery = "select GOODS_PRICE,GOODS_AMOUNT from ORDER_ITEM where GOODS_AMOUNT > 224000";

}
