/**
 * 
 */
package com.talentica.utility;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import com.talentica.tpc.table.TableEnum;

import io.ampool.client.AmpoolClient;
import io.ampool.monarch.table.Admin;

/**
 * The data uploader for BigDataBench generated data of e-com : ORDER and
 * ORDER_ITEM.
 * 
 * @author pooshans
 *
 */
public class AmpoolDataUploader {

	static JavaSparkContext jsc;

	public static void main(String[] args) {

		String master = null;
		String appName = null;
		String locatorHost = null;
		String locatorPort = null;
		String filePath = null;
		String fileName = null;
		int noOfPartition = 0;

		if (args.length == 6) {
			master = args[0];
			appName = args[1];
			locatorHost = args[2];
			locatorPort = args[3];
			filePath = args[4];
			fileName = args[5];
			noOfPartition = Integer.valueOf(args[6]);
		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<locatorHost> , <locatorPort> ,<filePath>,<file-name>");
			System.exit(-1);
		}
		final Properties props = new Properties();
		props.setProperty(io.ampool.conf.Constants.MClientCacheconfig.MONARCH_CLIENT_LOG, "/tmp/MTableClient.log");
		AmpoolClient aClient = new AmpoolClient(locatorHost, Integer.parseInt(locatorPort), props);
		Admin admin = aClient.getAdmin();
		try {
			Map<String, String> options = new HashMap<String, String>(3);
			options.put("ampool.locator.host", locatorHost);
			options.put("ampool.locator.port", locatorPort);

			/** create SparkSession **/
			SQLContext sqlContext = getSqlContext(master, appName);
			boolean isFtableExists;
			switch (fileName) {

			case "ORDER":
				String orderFilePath = filePath + "/" + "OS_ORDER.csv";
				isFtableExists = admin.existsFTable(TableEnum.OS_ORDER.getName());
				System.out.println(orderFilePath);
				System.out.println("OS_ORDER" + isFtableExists);
				if (admin.existsFTable(TableEnum.OS_ORDER.getName())) {
					admin.deleteFTable(TableEnum.OS_ORDER.getName());
					System.out.println("Deleted");
				}
				Dataset<Row> datasetOrder = sqlContext.read().format("com.databricks.spark.csv")
						.schema(Schema.getOrderTableSchema()).option("header", "false").option("delimiter", "|")
						.load(orderFilePath);
				Dataset<Row> newDatasetOrder = datasetOrder.repartition(noOfPartition);
				// newDatasetOrder.write().format("io.ampool").options(options).save(TableEnum.OS_ORDER.getName());
				newDatasetOrder.write().orc("hdfs://172.29.101.104:9000/" + TableEnum.OS_ORDER.getName());
				break;

			case "ORDER_ITEM":
				String orderItemFilePath = filePath + "/" + "OS_ORDER_ITEM.csv";
				isFtableExists = admin.existsFTable(TableEnum.OS_ORDER.getName());
				System.out.println("OS_ORDER_ITEM" + isFtableExists);
				if (admin.existsFTable(TableEnum.OS_ORDER_ITEM.getName())) {
					admin.deleteFTable(TableEnum.OS_ORDER_ITEM.getName());
					System.out.println("Deleted");
				}
				System.out.println(orderItemFilePath);
				Dataset<Row> datasetOrderItem = sqlContext.read().format("com.databricks.spark.csv")
						.schema(Schema.getOrderItemTableSchema()).option("header", "false").option("delimiter", "|")
						.load(orderItemFilePath);
				Dataset<Row> newDatasetOrderItem = datasetOrderItem.repartition(noOfPartition);
				/*
				 * newDatasetOrderItem.write().format("io.ampool").options(
				 * options) .save(TableEnum.OS_ORDER_ITEM.getName());
				 */
				newDatasetOrderItem.write().orc("hdfs://172.29.101.104:9000/" + TableEnum.OS_ORDER_ITEM.getName());
				break;

			default:
				System.err.println("Invaid file name");
				System.exit(-1);

			}
			jsc.stop();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static SQLContext getSqlContext(String master, String appName) {
		SparkConf conf = new SparkConf().setMaster(master).setAppName(appName);
		jsc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);
		return sqlContext;
	}

}
