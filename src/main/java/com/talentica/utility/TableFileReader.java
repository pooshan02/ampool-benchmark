/**
 * 
 */
package com.talentica.utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

/**
 * @author pooshans
 *
 */
public class TableFileReader implements Iterator<String[]> {

	private Reader fileReader;
	private BufferedReader bufferedReader;
	private String line;
	private String colDelimiter;
	private boolean isAvailable;
	private boolean hasNextCalled = false;

	public TableFileReader(String fileName, String colDelimiter) {
		try {
			fileReader = new FileReader(fileName);
			bufferedReader = new BufferedReader(fileReader);
			this.colDelimiter = colDelimiter;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean hasNext() {
		if(hasNextCalled) return isAvailable;
		try {
			line = bufferedReader.readLine();
			isAvailable = (line == null) ? false : true;
			hasNextCalled = true;
		} catch (IOException e) {
			e.printStackTrace();
			isAvailable = false;
		}
		return isAvailable;
	}

	@Override
	public String[] next() {
		hasNextCalled = false;
		return line.split(colDelimiter);
	}
	
	public void close(){
		if(fileReader != null){
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(bufferedReader != null){
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public boolean isAvailable(){
		return isAvailable;
	}
}
