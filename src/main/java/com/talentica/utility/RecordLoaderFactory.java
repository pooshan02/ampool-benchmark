/**
 * 
 */
package com.talentica.utility;

import java.util.ArrayList;
import java.util.List;

import com.talentica.ampool.tpc.spark.entity.Customer;
import com.talentica.ampool.tpc.spark.entity.LineItem;
import com.talentica.ampool.tpc.spark.entity.Nation;
import com.talentica.ampool.tpc.spark.entity.Orders;
import com.talentica.ampool.tpc.spark.entity.Part;
import com.talentica.ampool.tpc.spark.entity.PartSupp;
import com.talentica.ampool.tpc.spark.entity.Region;
import com.talentica.ampool.tpc.spark.entity.Supplier;

/**
 * @author pooshans
 *
 */
public class RecordLoaderFactory {
	
	private TableFileReader fileReader;
	
	public RecordLoaderFactory(final String fileName, String colDelimiter){
		fileReader = new TableFileReader(fileName, colDelimiter);
	}
	
	public List<Object> getPartRecords(String className,int batchSize){
		List<Object> records = new ArrayList<Object>(batchSize);
		int counter = 0;
		switch(className){
		case "PART" : 
			while(fileReader.hasNext()){
				records.add(new Part(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "SUPPLIER":
			while(fileReader.hasNext()){
				records.add(new Supplier(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "PARTSUPP":
			while(fileReader.hasNext()){
				records.add(new PartSupp(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "CUSTOMER":
			while(fileReader.hasNext()){
				records.add(new Customer(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "ORDERS":
			while(fileReader.hasNext()){
				records.add(new Orders(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "LINEITEM":
			while(fileReader.hasNext()){
				records.add(new LineItem(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "NATION":
			while(fileReader.hasNext()){
				records.add(new Nation(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
		case "REGION":
			while(fileReader.hasNext()){
				records.add(new Region(fileReader.next()));
				counter++;
				if(counter == batchSize) break;
			}
			break;
			default:
				System.err.println("Invalid argument");
				System.exit(-1);
		}
		if(!fileReader.hasNext()) {
			fileReader.close();
			System.out.println("Closing the stream connection");
		}
		return records;
	}
	
	public boolean isAvailable(){
		return fileReader.isAvailable();
	}

}
