/**
 * 
 */
package com.talentica.alluxio.bigdatabench.spark.sql;

import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.talentica.utility.QueryCommand;
import com.talentica.utility.Schema;

/**
 * @author pooshans
 *
 */
public class GroupByQuery {

	public static void main(String[] args) throws AnalysisException {
		String appName = null;
		String master = null;
		String alluxioBaseUrl = null;
		int numPartitions = -1;

		if (args.length == 4) {
			master = args[0];
			appName = args[1];
			alluxioBaseUrl = args[2];
			numPartitions = Integer.parseInt(args[3]);
		} else {
			System.err.println(
					"Please provide spark <master-URL> , <app-name> ,<alluxio-base-url> , <no-of-repartition {-1} for-default>");
			System.exit(-1);
		}

		/** create SparkSession **/
		SparkSession sparkSession = SparkSession.builder().appName(appName).master(master).getOrCreate();

		String alluxioOrderItemUrl = alluxioBaseUrl + "/" + "OS_ORDER_ITEM";
		Dataset<Row> orderItemDs = sparkSession.read().format("com.databricks.spark.csv").option("header", "false")
				.schema(Schema.getOrderItemTableSchema()).option("delimiter", "|").csv(alluxioOrderItemUrl);

		Dataset<Row> newOrderItemDs = (numPartitions != -1) ? orderItemDs.repartition(numPartitions) : orderItemDs;

		newOrderItemDs.createOrReplaceTempView("ORDER_ITEM");

		String query = QueryCommand.groupByQuery;
		Dataset<Row> resultDs = sparkSession.sql(query);
		resultDs.show();
		sparkSession.stop();

	}

}
